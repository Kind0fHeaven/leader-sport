$(document).ready(function() {

	const reviewsList = jQuery('.js_reviews-slider');

	reviewsList.slick({
		slidesToShow: 1,
	  	slidesToScroll: 1,
	  	arrows: true,
	  	dots: false,
	  	autoplay: true,
  		autoplaySpeed: 10000,
  		infinite: false,
	})

})