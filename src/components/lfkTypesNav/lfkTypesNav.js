jQuery(document).ready(function() {

	const navigationLinks = jQuery('.lfk-types-navigation .lfk-types-navigation__item');

	navigationLinks.click(function() {

		if (jQuery(this).hasClass('_active')) return 0;

		const href = jQuery(this).attr('data-href');

		jQuery(this).parent().find('.lfk-types-navigation__item._active').removeClass('_active');

		jQuery(this).addClass('_active');

		changeType(href);

	})

	function changeType(anchor) {
		jQuery('.lfk-types__single-type._active').removeClass('_active');
		jQuery(`.lfk-types__single-type[data-anchor=${anchor}]`).addClass('_active');
		const newText = jQuery(`.lfk-types-navigation__item[data-href=${anchor}]`).text();

		jQuery('.lfk-types-navigation').removeClass('_opened');
		jQuery('.lfk-types-navigation__header').text(newText);
	}
	

	jQuery('.lfk-types-navigation__header').click(function() {
		jQuery(this).parent().toggleClass('_opened')
	})
})

