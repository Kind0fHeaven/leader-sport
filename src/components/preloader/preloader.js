jQuery(window).on('load', function() {
	
	const body = jQuery('body');

	setTimeout(() => {
		body.removeClass('loading')
        if ($(".lfk-types-navigation__item[data-href=" + (document.location.hash.substr(1) || "''") + "]").length) {
            $("html, body").stop().animate({scrollTop: $('.lfk-types').offset().top}, 1500);
            $(".lfk-types-navigation__item[data-href=" + document.location.hash.substr(1) + "]").trigger('click')
        }
	}, 300)

})