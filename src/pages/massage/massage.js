jQuery(document).ready(function() {
	$('.js__sculpt-link').click(function() {
		const top = jQuery('.lfk-types').offset().top - 100;
		const link = jQuery(this).attr('data-link');
		jQuery('body,html').animate({'scrollTop': top}, 2000);
		$('.lfk-types-navigation__item[data-href="' + link + '"]').trigger('click');
	})
})